package ds2.gradle.cdi.plugin

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.SourceSetOutput
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File

class CdiPlugin : Plugin<Project> {
    private val log: Logger = LoggerFactory.getLogger(CdiPlugin::class.java)

    override fun apply(target: Project) {
        with(target) {
            val projectSources = this.property("sourceSets") as SourceSetContainer
            relinkResources(projectSources.getByName("main").output)
            relinkResources(projectSources.getByName("test").output)
        }
    }

    private fun relinkResources(sso: SourceSetOutput) {
        log.debug("Checking sources output {}", sso)
        val binDir: File = sso.classesDirs.singleFile
        log.info("The resources will be put into {}", binDir)
        sso.resourcesDir = binDir
    }

}
