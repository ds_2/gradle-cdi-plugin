package ds2.gradle.cdi.plugin

open class CdiPluginExtension {
    var implementationType: CdiImplementationType = CdiImplementationType.OWB
    var implementationVersion: String = ""

    override fun toString(): String {
        val sb: StringBuilder = StringBuilder(200)
        sb.append("{")
        sb.append("type=").append(implementationType)
        sb.append(", version=").append(implementationVersion)
        sb.append("}")
        return super.toString()
    }
}
