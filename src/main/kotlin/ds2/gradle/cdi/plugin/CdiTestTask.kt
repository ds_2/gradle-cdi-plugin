package ds2.gradle.cdi.plugin

import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.testing.Test
import java.util.function.Consumer

open class CdiTestTask : Test() {

    @TaskAction
    fun performTests(){
        logger.info("this instance: ${toString()}")
        executeTests()
    }

    override fun toString(): String {
        val sb:StringBuilder = StringBuilder(200)
        if(classpath!=null){
            classpath.forEach(
                    Consumer {
                        logger.info("cp: $it")
                    }
            )
        }

        //sb.append("classpath=$classpath")
        return sb.toString()
    }
}