package ds2.gradle.cdi.plugin

enum class CdiImplementationType {
    WELD, OWB
}