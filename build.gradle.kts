import net.researchgate.release.GitAdapter

plugins {
    id("org.jetbrains.kotlin.jvm").version("1.3.31")
    id("com.gradle.plugin-publish") version "0.10.1"
    id("java-gradle-plugin")
    id("maven-publish")
    id("net.researchgate.release") version "2.8.1"
}

repositories {
    mavenLocal()
    jcenter()
    mavenCentral()
}

dependencies {
    compile(gradleApi())
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    testImplementation("org.jetbrains.kotlin:kotlin-test")
    testImplementation("org.jetbrains.kotlin:kotlin-test-testng")
    testCompile("org.testng:testng:6.14.3")
    compile("org.apache.deltaspike.core:deltaspike-core-api:1.9.0")
    runtime("org.apache.deltaspike.core:deltaspike-core-impl:1.9.0")
    // Weld
    runtime("org.jboss.weld.se:weld-se:2.3.3.Final")
    runtime("org.apache.deltaspike.cdictrl:deltaspike-cdictrl-weld:1.9.0")
    //OWB
    runtime("org.apache.openwebbeans:openwebbeans-impl:1.6.3")
    compile("org.apache.openwebbeans:openwebbeans-spi:1.6.3")
    runtime("org.apache.deltaspike.cdictrl:deltaspike-cdictrl-owb:1.9.0")
}

pluginBundle {
    website = "https://gitlab.com/ds_2/gradle-cdi-plugin"
    vcsUrl = "https://gitlab.com/ds_2/gradle-cdi-plugin.git"
    tags = listOf("cdi", "openwebbeans", "weld", "arquillian")
}

gradlePlugin {
    plugins {
        create("p1") {
            id = "ds2.cdi"
            displayName = "DS/2 CDI Plugin"
            description = "A simple plugin, adding some configuration and tasks to allow cdi tests to run"
            implementationClass = "ds2.gradle.cdi.plugin.CdiPlugin"
        }
    }
}

publishing {
    publications {
        create<MavenPublication>("pluginMaven") {
            groupId = "ds2.gradle.plugins"
            artifactId = "ds2.gradle.cdi.plugin"
        }
    }
}

tasks.test {
    useTestNG()
    maxHeapSize = "1G"
}

release {
    tagTemplate = "v\${version}"
    with(propertyMissing("git") as GitAdapter.GitConfig) {
        requireBranch = "release/.+"
    }
}

tasks.afterReleaseBuild.configure {
    dependsOn("publishPlugins", "publishToMavenLocal")
}
