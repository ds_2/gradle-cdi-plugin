# DS/2 CDI Plugin for Gradle

A plugin that creates a new configuration to deal with CDI
lookups for testing.

## How to build locally

Execute the shell command:

    ./gradlew clean build

## (Developer) How to release

Invoke:

    ./gradlew clean release -Prelease.useAutomaticVersion=true